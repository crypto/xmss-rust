# eXtended Merkle Signature Scheme (XMSS)

Rust implementation according to [IETF RFC 8391](https://datatracker.ietf.org/doc/html/rfc8391).
